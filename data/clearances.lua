--- Table of from:to system names to checks

-- the key for each check pair is an event, and the value is the message to show
-- when you try to pass without that event having happened.

return {["Luyten's Star:Sol"] = {["background_check"] = "Interportal requires background check for security.\nPlease visit immigration at the nearest station to obtain a permit for travel."}}
