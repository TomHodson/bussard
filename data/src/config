-- This -*- lua -*- code is the entry point for your ship's computer's config.

-- It sets up the keys for flight mode and loads files containing definitions
-- of other modes.

local zoom_in = function(pressed, dt)
   if(pressed) then
      ship.scale = ship.scale - dt / ship.status.time_factor
   end
end

local zoom_out = function(pressed, dt)
   if(pressed) then
      ship.scale = ship.scale + dt / ship.status.time_factor
   end
end

-- controls are for keys that need to keep activating when held down.
ship.controls["up"] = ship.actions.forward
ship.controls["left"] = ship.actions.left
ship.controls["right"] = ship.actions.right
ship.controls["="] = zoom_in
ship.controls["-"] = zoom_out

---- Flight mode
define_mode("flight")

-- bind is for commands that only call their functions once even when held.
-- it takes the name of a mode, a key combo, and a function to run when pressed.
bind("flight", "escape", ship.ui.pause)

-- you can bind keys to existing functions or inline functions.
bind("flight", "ctrl-return", function()
        ship.editor.change_buffer("*console*")
end)

-- the mouse wheel is handled just like any other key press.
bind("flight", "wheelup", zoom_in)
bind("flight", "wheeldown", zoom_out)

-- regular tab selects next target in order of distance from the star.
bind("flight", "tab", ship.actions.next_target)

-- ctrl-tab selects next closest target in order of distance from your ship.
bind("flight", "ctrl-tab", ship.actions.closest_target)

-- quit auto-saves; don't worry.
bind("flight", "ctrl-q", ship.ui.quit)

-- contains configuration for heads-up display during flight mode.
dofile("src.hud")

-- other modes
dofile("src.edit") -- editor
dofile("src.mail") -- mail client
dofile("src.console") -- console for interacting with ship's computer
dofile("src.ssh") -- ssh mode for interacting with station/planet computers

-- bindings can affect more than one mode at a time:
bind({"flight", "edit"}, "ctrl-m", mail)

-- to open a file.
bind({"flight", "edit"}, "ctrl-o", function()
      -- this is more complicated because it offers live feedback as you type.
      local callback = function(input, cancel)
         -- open the file is given, but we don't support opening tables yet
         if(not cancel and type(ship:find(input)) ~= "table") then
            ship.editor.open(ship, input)
         end
      end
      -- show completions as you go
      local completer = function(input)
         local completions = utils.completions_for(input, ship, ".")
         -- different types should display differently
         local decorate_type = function(path)
            local x = ship:find(path)
            if(type(x) == "table") then
               return path .. "."
            elseif(type(x) == "string") then
               return path
            end
         end
         -- filter out non-table, non-string entries
         return lume.filter(lume.map(completions, decorate_type))
      end
      -- read_line takes a callback function that is called with input.
      ship.editor.read_line("Open: ", callback, {completer=completer})
end)

-- reload config
bind({"flight", "edit"}, "ctrl-r",
   function()
      ship.dofile("src.config")
      print("Successfully reloaded config.")
end)

-- connect to target over ssh.
bind("flight", "ctrl-s", ssh)

-- activate portal sequence.
bind("flight", "ctrl-p", portal)

-- If you keep your customizations in this file, they will be preserved even if
-- you start a new game, and they can be edited in an external editor; all files
-- under "host." are stored in the host_fs/ directory in the game save directory
-- dofile("host.config")
